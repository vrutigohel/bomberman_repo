﻿/*
 * Copyright (c) 2017 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish, 
 * distribute, sublicense, create a derivative work, and/or sell copies of the 
 * Software in any work that is designed, intended, or marketed for pedagogical or 
 * instructional purposes related to programming, coding, application development, 
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works, 
 * or sale is expressly withheld.
 *    
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GlobalStateManager : MonoBehaviour
{
	public static GlobalStateManager Instance;
	public List<GameObject> Players = new List<GameObject> ();

    public int deadPlayers = 0;
    private int deadPlayerNumber = -1;

	public GameObject DestructiveWall;
	public GameObject Enemy;
	public GameObject BlocksParent;
	public GameObject PickupParent;
	public GameObject Pickups;
	public GameObject GameWinPanel;
	public int NoMatch;
	void Awake()
	{
		if (Instance == null)
			Instance = this;
	}
	void Start()
	{
		NoMatch = 0;
//		for (int i = 0; i < 500; i++) {
//			if (BlocksParent.transform.childCount <= 30)
//				StartCoroutine (GenerateWall ());
//		}
		StartCoroutine (GenerateWall ());
	}

	/// <summary>
	/// Generates destructible the wall.
	/// </summary>
	/// <returns>The wall.</returns>
	public IEnumerator GenerateWall()
	{

		// 1 logic with random x and y 
//		int xpoz = Random.Range (1, 9);
//		int zpoz = Random.Range (2, 9);
//		for (int i = 0; i < BlocksParent.transform.childCount; i++) {
//			if((float)xpoz != BlocksParent.transform.GetChild(i).transform.position.x && ((float)zpoz-1) != BlocksParent.transform.GetChild(i).transform.position.z)
//			{	
//				NoMatch++;
//			}
//		}
//
//		if (NoMatch >= BlocksParent.transform.childCount) {
////			Debug.Log ("childcount::"+BlocksParent.transform.childCount);
////			Debug.Log(xpoz+"::XZ::"+zpoz);
//			GameObject Dest_Wall = Instantiate (DestructiveWall, new Vector3 ((float)xpoz, 0.5f, (float)zpoz-1), DestructiveWall.transform.rotation) as GameObject;
//			Dest_Wall.transform.parent = BlocksParent.transform;
//		} 
//
//		Debug.Log (NoMatch);
//		NoMatch = 0;
		yield return new WaitForSeconds (0.02f);

		//2nd logic for random x and y
		int xpoz = Random.Range (1, 9);
		for (int z = 2; z < 10;) {
			GameObject Dest_Wall = Instantiate (DestructiveWall, new Vector3 ((float)xpoz, 0.5f, (float)z - 1), DestructiveWall.transform.rotation) as GameObject;
			Dest_Wall.transform.parent = BlocksParent.transform;
			z += 2;
		}

		int zpoz = Random.Range (2, 9);
		for (int x = 1; x < 10;) {
			GameObject Dest_Wall = Instantiate (DestructiveWall, new Vector3 ((float)x, 0.5f, (float)zpoz - 1), DestructiveWall.transform.rotation) as GameObject;
			Dest_Wall.transform.parent = BlocksParent.transform;
			if(x==3)
			x += 1;
			else
				x += 2;
		}

		yield return new WaitForSeconds (1.0f);
		for (int i = 0; i < 4; i++) {
			yield return new WaitForSeconds (0.02f);
			EnemyGeneration (xpoz);
		}

	}
	public void EnemyGeneration(int poz){
		if (poz == 8 || poz == 1)
			poz = 3;
		int _zpoz = Random.Range (3,8);
		GameObject Dest_Enemy = Instantiate (Enemy, new Vector3 ((float)poz+1, 0.5f, (float)_zpoz), Enemy.transform.rotation) as GameObject;
		Dest_Enemy.AddComponent<EnemyManage> ();	
		Dest_Enemy.transform.parent = BlocksParent.transform;
	}
	public void PlayerDied (int playerNumber)
    {
        deadPlayers++;

        if (deadPlayers == 1)
        {
            deadPlayerNumber = playerNumber;
            Invoke ("CheckPlayersDeath", .3f);
        }
    }

    void CheckPlayersDeath ()
    {
        if (deadPlayers == 1)
        { //Single dead player, he's the winner
			GameWinPanel.SetActive (true);
            if (deadPlayerNumber == 1)
            { //P1 dead, P2 is the winner
                Debug.Log ("Player 2 is the winner!");
				GameWinPanel.transform.GetChild(0).GetComponent<Text>().text = "Player 2 winner!";
				Time.timeScale = 0;
            } else
            { //P2 dead, P1 is the winner
                Debug.Log ("Player 1 is the winner!");
				GameWinPanel.transform.GetChild(0).GetComponent<Text>().text = "Player 1 winner!";
				Time.timeScale = 0;
            }
        } else
        {  //Multiple dead players, it's a draw
            Debug.Log ("The game ended in a draw!");
        }
    }
	public void pickup_appear(Vector3 poz)
	{
		GameObject PickupObject	= Instantiate (Pickups,poz,Quaternion.identity) as  GameObject;
		int pickup_number = Random.Range (1,5);
		PickupObject.transform.tag = "pickup" + pickup_number.ToString ();
		PickupObject.transform.parent = PickupParent.transform;
	}
	public void playagain()
	{
		Time.timeScale = 1;
		Application.LoadLevelAsync (0);
	}

}
