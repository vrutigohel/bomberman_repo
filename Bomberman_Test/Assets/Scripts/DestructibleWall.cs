﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleWall : MonoBehaviour {
	public void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag ("Explosion"))
		{ 
			Debug.Log (" Destructible wall hit by explosion!");
			GlobalStateManager.Instance.pickup_appear (transform.position);
			Destroy (gameObject);
		}
	}

}
