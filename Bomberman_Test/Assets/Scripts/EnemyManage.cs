﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManage : MonoBehaviour {
	private Rigidbody rigidBody;
	float enemySpeed = 1f;
	int XYdir = 0;
	void Start()
	{
		rigidBody = GetComponent<Rigidbody> ();
	}
	public void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag ("Block"))
		{ 
			enemySpeed = -enemySpeed;

			XYdir = Random.Range (0, 2);
		}
		if (other.CompareTag ("Explosion")) {
			Destroy (gameObject);
		}
	}
	void Update()
	{
		if(XYdir == 0)
		rigidBody.velocity = new Vector3 (enemySpeed, rigidBody.velocity.y, rigidBody.velocity.z);
		else
			rigidBody.velocity = new Vector3 (rigidBody.velocity.z, rigidBody.velocity.y, enemySpeed);	
	}
}
