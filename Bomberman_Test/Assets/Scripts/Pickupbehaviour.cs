﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickupbehaviour : MonoBehaviour {

	public void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag ("Player"))
		{ 
			Debug.Log (" pickup by player!");
			if (other.name.Contains ("1"))
				ManagePickup (0);
			else
				ManagePickup (1);


			//	GlobalStateManager.Instance;
			Destroy (gameObject);
		}
	}
	void ManagePickup(int playernam)
	{
		switch (transform.tag) {
		case "pickup1":
			GlobalStateManager.Instance.Players [playernam].GetComponent<Player> ().ExplosionBombLength++;
			break;
		case "pickup2":
			GlobalStateManager.Instance.Players [playernam].GetComponent<Player> ().Bomblimit++;
			break;
		case "pickup3":
			GlobalStateManager.Instance.Players [playernam].GetComponent<Player> ().moveSpeed += 2;
			break;
		case "pickup4":
			GlobalStateManager.Instance.Players [playernam].GetComponent<Player> ().explodeTime = 10f;
			break;
		}
	}
}
