# README #

### What is this repository for? ###

* This is for bombermantest
* initialize project with readme and .gitignore file

* I am going to implement this project by separating in to four parts.
* 1. Couch Co-Op
* 2. Destructible wall
* 3. Pickups
* 4. Enemy AI

### 1. Couch Co-Op

* I have created logic for couch co-op :
*      1.Player 1's movement and facing rotation using the WASD keys
*      2.Updates Player 2's movement and facing rotation using the arrow keys
* downloaded some models(Players,Bombs) and particles from assetstore.
* created one animator controller for player.
* implemented dropping a bomb functionality:
*      1.player 1 can drop bomb by space key
*      2.player 2 can drop bomb by enter key

### 2. Destructible Wall 

* created a logics for destructible walls that generate at random place other then static wall
* blue color is for destructible and yellow(Dirty fellow ;-)) is for static wall.

### 3. Pickups

* created 4 types of  pickups
* 1. Speed incresed
* 2. More Bombs
* 3. Bomb Explosion length increased
* 4. Self controlled bomb droping :
*   --> player 1 can controlled by using leftctrl 
*   --> player 2 can controlled by using rightctrl

### 4.EnemyAI
* implementeda logic for AI that randomly move in area.if player touches player die.
* if explosion touches, enemy die0.


* implemented a logic for pause game when one of the player dies.
* Players can play again.
